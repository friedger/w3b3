export default class Address {
  constructor(options = {}) {
    this.type = options.type || "";
    this.street1 = options.street1 || "";
    this.street2 = options.street2 || "";
    this.city = options.city || "";
    this.state = options.state || "";
    this.country = options.country || "";
    this.postalCode = options.postalCode || "";
    this.annotation = options.annotation || "";
  }
}
