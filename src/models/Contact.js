import shortid from "shortid";

export default class Contact {
  constructor(options = {}) {
    this.id = shortid.generate();
    this.firstName = options.firstName || "";
    this.lastName = options.lastName || "";
    this.middleName = options.middleName || "";
    this.blockstackID = options.blockstackID || "";
    this.assistantName = options.assistantName || "";
    this.birthday = options.birthday || "";
    this.businessHomePage = options.businessHomePage || "";
    this.companyName = options.companyName || "";
    this.department = options.department || "";
    this.displayName = options.displayName || "";
    this.jobTitle = options.jobTitle || "";
    this.createdDateTime = Date.now();
    this.lastModifiedDateTime = Date.now();
    this.notes = options.notes || "";
    this.spouseName = options.partnerName || "";
    this.website = options.website || "";
    this.phones = options.phones || [];
    this.addresses = options.addresses || [];
    this.emails = options.emails || [];
    this.accounts = options.accounts || [];
    this.photo = options.photo || "";
    this.children = options.children || [];
    this.isStarred = options.isStarred || false;
  }
}
