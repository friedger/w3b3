const routes = [
  { path: "/", component: () => import("pages/Index.vue") },
  {
    path: "/webby",
    component: () => import("layouts/MyLayout.vue"),
    meta: { requiresAuth: true },
    children: [
      {
        path: "",
        component: () => import("pages/Dapps.vue")
      },
      {
        path: "category/:id",
        component: () => import("pages/Categories.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
