import faker from "faker";

export default async ({ Vue }) => {
  Vue.prototype.$faker = faker;
};

export { faker };
