/* eslint-disable no-undef */

import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
// var merge = require("lodash.merge");

import { userSession } from "boot/blockstack";

// we first import the module
// import app from "./app";
// import auth from "./auth";
// import dapps from "./dapps";

Vue.use(Vuex);

let state = {
  searchTerm: "",
  loading: false,
  categories: [],
  apps: [],
  favorites: [],
  user: null
};

let actions = {
  setSearchTerm(context, input) {
    context.commit("SET_SEARCHTERM", input.target.value);
  },
  setLoadingStatus(context, status) {
    context.commit("SET_LOADING_STATUS", status);
  },
  async init(context) {
    let response = (await Vue.prototype.$axios.get("/statics/data/apps.json"))
      .data;
    // let metadata = (await Vue.prototype.$axios.get(
    //   "/statics/data/app-meta-data.json"
    // )).data;
    let apps = response.apps;
    apps = apps.filter(app => app.category !== "Sample Blockstack Apps");
    // apps = merge(apps, metadata);
    console.log("unioned app data: ", apps);
    context.commit("INIT_APPS", apps);
    let categories = [...new Set(apps.map(app => app.category))];
    categories = categories.sort();
    context.commit("INIT_CATEGORIES", categories);
    let data = await userSession.getFile("webby-favorites.json");
    let faves = await (JSON.parse(data) || []);
    context.commit("SET_SAVED_FAVORITES", faves);
  },
  addFavorite(context, id) {
    context.commit("ADD_FAVORITE", id);
  },
  removeFavorite(context, id) {
    context.commit("REMOVE_FAVORITE", id);
  },
  setFaves(context, faves) {
    context.commit("SET_SAVED_FAVORITES", faves);
    // let newFaves = faves.map(fave => fave.id);
    // userSession.putFile("webby-favorites.json", JSON.stringify(newFaves));
  },
  setUser(context, user) {
    context.commit("SET_USER", user);
  }
};

let mutations = {
  SET_SEARCHTERM(state, term) {
    state.searchTerm = term;
  },
  SET_LOADING_STATUS(state, status) {
    state.loading = status;
  },
  INIT_APPS(state, apps) {
    state.apps = apps;
  },
  INIT_CATEGORIES(state, cats) {
    state.categories = cats;
  },
  INIT_APP_METADATA(state, metadata) {
    state.appMetaData = metadata;
  },
  ADD_FAVORITE(state, id) {
    state.favorites.push(id);
  },
  REMOVE_FAVORITE(state, id) {
    state.favorites.splice(state.favorites.indexOf(id), 1);
  },
  SET_SAVED_FAVORITES(state, faves) {
    console.log("mutation SET_SAVED_FAVORITES: ", faves);
    state.favorites = faves;
  },
  SET_USER(state, user) {
    state.user = user;
  }
};

let getters = {
  searchTerm(state) {
    return state.searchTerm;
  },
  loading(state) {
    return state.loading;
  },
  apps(state) {
    return state.apps;
  },
  categories(state) {
    return state.categories;
  },
  filteredAppsByCategory(state) {
    const re = new RegExp(state.searchTerm, "i");
    let filterResult = state.categories.map(category => ({
      label: category,
      apps: state.apps.filter(
        app => app.category === category && re.test(app.name)
      )
    }));

    filterResult = filterResult.filter(res => res.apps.length > 0);
    return filterResult;
  },
  favorites(state) {
    let faves = [];
    // return state.apps.filter(app => state.favorites.includes(app.id));
    state.favorites.forEach(val => {
      faves.push(state.apps.find(app => app.id === val));
    });
    return faves;
  },
  favoriteIDs(state) {
    return state.favorites;
  },
  user(state) {
    return state.user;
  }
};

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
    plugins: [createPersistedState({ paths: ["app.user"] })],
    // modules: {
    // then we reference it
    // app
    // auth,
    // dapps
    // },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  /*
    if we want some HMR magic for it, we handle
    the hot update like below. Notice we guard this
    code with "process.env.DEV" -- so this doesn't
    get into our production build (and it shouldn't).
  */

  // if (process.env.DEV && module.hot) {
  //   module.hot.accept(["./app"], () => {
  //     const newApp = require("./app").default;
  //     Store.hotUpdate({ modules: { app: newApp } });
  //   });
  // }

  return Store;
}
