import { register } from "register-service-worker";
import { Notify, Loading } from "quasar";

// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  registrationOptions: { scope: "./" },

  ready() {
    console.log("App is being served from cache by a service worker.");
  },

  registered(registration) {
    console.log("Service worker has been registered.", registration);
  },

  cached(registration) {
    console.log("Content has been cached for offline use.", registration);
  },

  updatefound(registration) {
    console.log("New content is downloading.", registration);
  },

  updated(registration) {
    console.log("New content is available; please refresh.");

    Notify.create({
      message: "Webby just got better!",
      timeout: 10000, // You can adjust this, use 0 for infinite
      closeBtn: "Close",
      actions: [
        {
          label: "Update",
          icon: "img:/statics/icons/favicon-32x32.png",
          color: "white",
          handler() {
            navigator.serviceWorker.addEventListener("controllerchange", () => {
              window.location = "/";
            });

            // This process if rather fast generally, but for better experience show "Loading"
            Loading.show({
              delay: 0,
              message: "Updating the app..."
            });
            registration.waiting.postMessage({ type: "SKIP_WAITING" });
          }
        }
      ]
    });
  },
  offline() {
    console.log(
      "No internet connection found. App is running in offline mode."
    );
  },

  error(err) {
    console.error("Error during service worker registration:", err);
  }
});
